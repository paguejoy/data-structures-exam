# Data Structures Technical Exam

## Instructions
1. Clone this project from your batch's **resources** folder.
2. Create a remote repository in your subgroup within the class (i.e. https://gitlab.com/tuitt/students/batch92/john-doe) named DataStructuresExam.
3. Push your submission to this remote repository.

## Note
* To start using the built-in tests, run <code>npm install</code> first then issue the <code>npm test</code> at the root directory of this project.

## WARNING 
Do not change any code inside <code>test.js</code>.